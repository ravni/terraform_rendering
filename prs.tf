resource azurerm_kubernetes_cluster "k8s_cluster_vul" {
  name                = "terragoat-aks-${var.environment}-vul"
  resource_group_name = azurerm_resource_group.example.name
  identity {
    type = "SystemAssigned"
  }
  default_node_pool {
    name       = "default"
    vm_size    = "Standard_D2_v2"
    node_count = 2
  }
  addon_profile {
    oms_agent {
      enabled = false
    }
    kube_dashboard {
      enabled = true
    }
  }
  role_based_access_control {
    enabled = false
  }
}


resource azurerm_kubernetes_cluster "k8s_cluster_comp1" {
  name                = "terragoat-aks-${var.environment}-comp1"
  resource_group_name = azurerm_resource_group.example.name
  identity {
    type = "SystemAssigned"
  }
  default_node_pool {
    name       = "default"
    vm_size    = "Standard_D2_v2"
    node_count = 2
  }
  addon_profile {
    oms_agent {
      enabled = false
    }
    kube_dashboard {
      enabled = true
    }
  }
  role_based_access_control {
    enabled = false
  }
  disk_encryption_set_id = "test"
}


resource azurerm_kubernetes_cluster "k8s_cluster_comp2" {
  name                = "terragoat-aks-${var.environment}-comp2"
  resource_group_name = azurerm_resource_group.example.name
  identity {
    type = "SystemAssigned"
  }
  default_node_pool {
    name       = "default"
    vm_size    = "Standard_D2_v2"
    node_count = 2
  }
  addon_profile {
    oms_agent {
      enabled = false
    }
    kube_dashboard {
      enabled = true
    }
  }
  role_based_access_control {
    enabled = false
  }
  disk_encryption_set_id = "test"
}


resource azurerm_kubernetes_cluster "k8s_cluster_comp3" {
  name                = "terragoat-aks-${var.environment}-comp3"
  resource_group_name = azurerm_resource_group.example.name
  identity {
    type = "SystemAssigned"
  }
  default_node_pool {
    name       = "default"
    vm_size    = "Standard_D2_v2"
    node_count = 2
  }
  addon_profile {
    oms_agent {
      enabled = false
    }
    kube_dashboard {
      enabled = true
    }
  }
  role_based_access_control {
    enabled = false
  }
  disk_encryption_set_id = "test"
}



resource "aws_cloudwatch_log_group" "not_encrypted" {
}




