resource "aws_rds_cluster" "rds_cluster_bad" {
  deletion_protection = true
  iam_database_authentication_enabled = true
  storage_encrypted = true
  backup_retention_period = 0
  
  
  tags = {
    Name = "test smart fixes"
  }
}


resource "aws_rds_cluster" "rds_cluster_pass1" {
  deletion_protection = true
  iam_database_authentication_enabled = true
  storage_encrypted = true
  backup_retention_period = 7
  
  
  tags = {
    Name = "test smart fixes"
  }
}

resource "aws_rds_cluster" "rds_cluster_pass11" {
  deletion_protection = true
  iam_database_authentication_enabled = true
  storage_encrypted = true
  backup_retention_period = 7
  
  
  tags = {
    Name = "test smart fixes"
  }
}


resource "aws_rds_cluster" "rds_cluster_pass2" {
  deletion_protection = true
  iam_database_authentication_enabled = true
  storage_encrypted = true
  backup_retention_period = 14
  
  
  tags = {
    Name = "test smart fixes"
  }
}

resource "aws_rds_cluster" "rds_cluster_pass22" {
  deletion_protection = true
  iam_database_authentication_enabled = true
  storage_encrypted = true
  backup_retention_period = 14
  
  
  tags = {
    Name = "test smart fixes"
  }
}


resource "aws_rds_cluster" "rds_cluster_pass222" {
  deletion_protection = true
  iam_database_authentication_enabled = true
  storage_encrypted = true
  backup_retention_period = 14
  
  
  tags = {
    Name = "test smart fixes"
  }
}


resource "aws_rds_cluster" "rds_cluster_pass3" {
  deletion_protection = true
  iam_database_authentication_enabled = true
  storage_encrypted = true
  backup_retention_period = 21
  
  
  tags = {
    Name = "test smart fixes"
  }
}


resource "aws_rds_cluster" "rds_cluster_pass33" {
  deletion_protection = true
  iam_database_authentication_enabled = true
  storage_encrypted = true
  backup_retention_period = 21
  
  
  tags = {
    Name = "test smart fixes"
  }
}

resource "aws_rds_cluster" "rds_cluster_pass333" {
  deletion_protection = true
  iam_database_authentication_enabled = true
  storage_encrypted = true
  backup_retention_period = 21
  
  
  tags = {
    Name = "test smart fixes"
  }
}


resource "aws_rds_cluster" "rds_cluster_pass3333" {
  deletion_protection = true
  iam_database_authentication_enabled = true
  storage_encrypted = true
  backup_retention_period = 21
  
  
  tags = {
    Name = "test smart fixes"
  }
}


resource "aws_backup_plan" "example" {
  name = "tf_example_backup_plan"

  rule {
    rule_name         = "tf_example_backup_rule"
    target_vault_name = "vault-name"
    schedule          = "cron(0 12 * * ? *)"
  }
}

resource "aws_backup_selection" "backup_good" {
  iam_role_arn = "arn:partition:service:region:account-id:resource-id"
  name         = "tf_example_backup_selection"
  plan_id      = aws_backup_plan.example.id

  resources = [
    aws_rds_cluster.rds_cluster_bad.arn,
    aws_rds_cluster.rds_cluster_pass1.arn,
    aws_rds_cluster.rds_cluster_pass11.arn,
    aws_rds_cluster.rds_cluster_pass2.arn,
    aws_rds_cluster.rds_cluster_pass22.arn,
    aws_rds_cluster.rds_cluster_pass222.arn,
    aws_rds_cluster.rds_cluster_pass3.arn,
    aws_rds_cluster.rds_cluster_pass33.arn,
    aws_rds_cluster.rds_cluster_pass333.arn,
    aws_rds_cluster.rds_cluster_pass3333.arn,
    
  ]
}
