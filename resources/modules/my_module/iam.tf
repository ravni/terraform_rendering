resource "aws_iam_user_policy" "user-policy" {
  count = 1
  policy = var.policy
  user = "my violating user"
}