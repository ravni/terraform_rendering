module "example_module" {
  source = "../modules/my_module"
}

resource "aws_iam_user_policy" "user-policy-ok1" {
  count = 1
  policy = var.policy
}


resource "aws_iam_user_policy" "user-policy-ok2" {
  count = 1
  policy = var.policy
}


resource "aws_iam_user_policy" "user-policy-ok3" {
  count = 1
  policy = var.policy
}