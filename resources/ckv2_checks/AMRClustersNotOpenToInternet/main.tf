resource "aws_emr_cluster" "cluster_ok" {
  name          = "emr-test-arn"
  release_label = "emr-4.6.0"
  applications  = ["Spark"]

  ec2_attributes {
    emr_managed_master_security_group = aws_security_group.block_access_ok.id
    emr_managed_slave_security_group  = aws_security_group.block_access_ok.id
    instance_profile                  = "connected_to_aws_iam_instance_profile"
  }
  tags = {
    git_commit           = "f991ab2718823a8566f22705dc1bf280e2d75a4a"
    git_file             = "resources/ckv2_checks/AMRClustersNotOpenToInternet/main.tf"
    git_last_modified_at = "2021-04-05 07:29:33"
    git_last_modified_by = "rotemavn@gmail.com"
    git_modifiers        = "rotemavn2"
    git_org              = "rotemavni"
    git_repo             = "terraform_rendering"
    yor_trace            = "09ea4ba8-d8a9-4242-a2e4-17d1ab171ea5"
  }
}

resource "aws_security_group" "block_access_ok" {
  name        = "block_access"
  description = "Block all traffic"

  ingress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["10.0.0.1/10"]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["10.0.0.10/10"]
  }
  tags = {
    rotem                = "bana2"
    git_commit           = "e5cb1768898442e0ece550201adf1684d645d54f"
    git_file             = "resources/ckv2_checks/AMRClustersNotOpenToInternet/main.tf"
    git_last_modified_at = "2021-04-08 14:32:08"
    git_last_modified_by = "rotemavn@gmail.com"
    git_modifiers        = "rotemavn"
    git_org              = "rotemavni"
    git_repo             = "terraform_rendering"
    yor_trace            = "6c406d3c-74f1-4d99-af65-7cfe37f87223"
  }
}

resource "aws_emr_cluster" "cluster_not_connected" {
  name          = "emr-test-arn"
  release_label = "emr-4.6.0"
  applications  = ["Spark"]

  ec2_attributes {
    instance_profile = "connected_to_aws_iam_instance_profile"
  }
  tags = {
    git_commit           = "75348626ac5b92d20eb9a10731d45fa9bf35fabc"
    git_file             = "resources/ckv2_checks/AMRClustersNotOpenToInternet/main.tf"
    git_last_modified_at = "2021-04-08 07:34:48"
    git_last_modified_by = "rotemavn@gmail.com"
    git_modifiers        = "rotemavn"
    git_org              = "rotemavni"
    git_repo             = "terraform_rendering"
    yor_trace            = "94d9b11f-c3f7-4ff7-8c89-4105ea3d635c"
  }
}


resource "aws_emr_cluster" "cluster_connected_to_wrong_group" {
  name          = "emr-test-arn"
  release_label = "emr-4.6.0"
  applications  = ["Spark"]

  ec2_attributes {
    emr_managed_master_security_group = aws_security_group.block_access_not_ok.id
    emr_managed_slave_security_group  = aws_security_group.block_access_not_ok.id
    instance_profile                  = "connected_to_aws_iam_instance_profile"
  }
  tags = {
    git_commit           = "f991ab2718823a8566f22705dc1bf280e2d75a4a"
    git_file             = "resources/ckv2_checks/AMRClustersNotOpenToInternet/main.tf"
    git_last_modified_at = "2021-04-05 07:29:33"
    git_last_modified_by = "rotemavn@gmail.com"
    git_modifiers        = "rotemavn"
    git_org              = "rotemavni"
    git_repo             = "terraform_rendering"
    yor_trace            = "f9398483-9f29-4171-8c26-74c4636cccf0"
  }
}

resource "aws_security_group" "block_access_not_ok" {
  name        = "block_access"
  description = "Block all traffic"

  ingress {
    from_port   = 0
    to_port     = 65535
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port   = 80
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
  tags = {
    git_commit           = "75348626ac5b92d20eb9a10731d45fa9bf35fabc"
    git_file             = "resources/ckv2_checks/AMRClustersNotOpenToInternet/main.tf"
    git_last_modified_at = "2021-04-08 07:34:48"
    git_last_modified_by = "rotemavn@gmail.com"
    git_modifiers        = "rotemavn"
    git_org              = "rotemavni"
    git_repo             = "terraform_rendering"
    yor_trace            = "4e087ad0-6505-4914-ad9c-a96bef21880e"
  }
}
